@extends('layouts.app')

@section('content')
  <section id="slider" class="slider">
    <div class="swiper-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-image: url(@asset('images/slide1.jpg'))">
          <div class="slider--content">
            <div class="container">
              <h2>WE CARE ABOUT YOUR <span>HEALTH</span></h2>
              <p>
                Health care is the diagnosis, treatment,and prevention of disease, illness,
                injury. Our emergency department is a medical treatment and
                specializing in emergency medicine.
              </p>
              <div class="slider--content---footer">
                <a class="btn btn-action text-white">CTA button</a>
                <a class="btn btn-hollow text-white">Hollow CTA button</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container" data-aos="fade-up">
      <div class="swiper-nav">
        <div class="swiper-nav--item">
          <div class="svg">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 59.99 59.99"><g fill="#FFF"><path d="M23.265 18.623l-1.303.414a1 1 0 0 0 .606 1.906l.697-.221v3.902l-1.303.414a1 1 0 0 0 .606 1.906l.697-.221v3.902l-1.303.414a1 1 0 0 0 .606 1.906l.697-.221v2.268h2v3.333l3.5 4.667h3l3.5-4.667V34.99h2v-6.613l1.316-.439a.999.999 0 1 0-.633-1.897l-.684.228v-3.892l1.316-.439a.999.999 0 1 0-.633-1.897l-.684.228v-3.892l1.316-.439a.988.988 0 0 0 .671-.948h16.473l-.106-1.097c-.099-1.021-.254-1.482-.435-2.019-.087-.26-.188-.559-.304-.995-.818-3.095-3.213-6.049-6.742-8.318C43.624-.341 37.326-.82 32.094 1.345l-.19.081c-.095.041-.188.082-.286.116-.465.161-.822.3-1.113.413-.922.357-.921.358-2.441-.09-3.245-.958-9.39-2.314-14.086-.649-5.744 2.035-8.912 6.306-9.417 12.695l-.085 1.079h18.789v3.633zM6.671 12.99c.685-4.993 3.302-8.233 7.975-9.89 4.32-1.529 10.428-.031 12.853.683 1.991.589 2.305.591 3.731.036.273-.106.608-.236 1.042-.388.143-.049.284-.108.427-.17l.16-.068c4.645-1.922 10.219-1.509 14.199 1.05 3.108 1.999 5.2 4.537 5.891 7.148.13.491.243.828.342 1.121.059.174.108.322.152.478H35.265v1.947l-2.316.772a.999.999 0 1 0 .633 1.897l1.684-.561v3.893l-2.316.772a.999.999 0 1 0 .633 1.897l1.684-.561v3.893l-2.316.772a.999.999 0 1 0 .633 1.897l1.684-.561v3.946h-2v4.667l-2.5 3.333h-1l-2.5-3.333v-4.67h-2v-.903l4.303-1.366a1 1 0 0 0-.606-1.906l-3.697 1.174v-3.901l4.303-1.366a1 1 0 0 0-.606-1.906l-3.697 1.174v-3.901l4.303-1.366a1 1 0 0 0-.606-1.906l-3.697 1.174V12.99H6.671z"/><path d="M55.309 18.99H42.265v2H53.48c.408 6.189-1.127 12.199-2.614 18.02-.459 1.795-.934 3.651-1.345 5.488l-.29 1.31c-.78 3.551-1.587 7.223-3.394 10.39-.608 1.065-1.232 1.691-1.647 1.632-.458-.047-1.724-.771-3.569-6.456a445.436 445.436 0 0 1-2.313-7.395 3.304 3.304 0 0 0-.043-.13l-.038-.133-.012.004c-.325-.77-.841-.809-1.233-.692l-.949.284.185.647c-.458 1.259-1.194 2.735-1.742 3.834l-.464.939c-.699 1.44-1.57 3.232-2.978 3.649-2.515.753-4.827-3.408-6.207-5.891-.212-.382-.406-.73-.581-1.028l-.333-.551-.625.05c-.51.04-1.144.09-4.771 9.399-.64 1.642-1.239 2.96-2.512 3.607-1.134-.283-2.345-3.231-2.815-4.376l-.121-.294C9.679 45.11 8.34 35.914 7.046 27.02c-.183-1.256-.364-2.505-.551-3.743l-.064-.424c-.135-.884-.185-1.207-.158-1.864h11.994v-2H4.41l-.073.921c-.126 1.597-.091 1.9.115 3.246l.064.418c.186 1.236.367 2.481.549 3.734 1.312 9.016 2.668 18.338 6.154 26.755l.119.288c.866 2.108 2.315 5.638 4.837 5.638h.203l.187-.079c2.314-.98 3.18-3.201 3.813-4.823a135.944 135.944 0 0 1 3.006-7.074c1.699 3.027 4.402 7.413 8.216 6.286 2.253-.668 3.383-2.995 4.208-4.693l.454-.921c.26-.521.561-1.124.858-1.755.603 1.95 1.137 3.646 1.597 5.06 1.21 3.728 2.782 7.573 5.267 7.829 1.312.134 2.515-.751 3.588-2.631 1.958-3.432 2.798-7.254 3.61-10.951l.288-1.302c.405-1.809.876-3.649 1.331-5.431 1.603-6.271 3.259-12.754 2.594-19.611l-.086-.903z"/></g></svg>
          </div>
          <h2>NEUROLOGY CARE</h2>
          <p>
            A neurologist is a physician and
            specializing in neurology
          </p>
        </div>
        <div class="swiper-nav--item">
          <div class="svg">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56.599 56.599"><g fill="#FFF"><path d="M53.065 10.657c-.818-3.096-3.213-6.05-6.742-8.317-4.35-2.796-10.497-3.11-16.047-.813l-.182.077c-.097.042-.191.084-.292.119-.465.161-.822.3-1.113.413-.923.356-.922.357-2.44-.09C21.914.769 16.398-.507 12.162.993c-5.73 2.031-10.809 8.753-9.238 19.19.187 1.235.367 2.48.549 3.731 1.313 9.016 2.668 18.339 6.155 26.758l.12.291c.865 2.107 2.314 5.635 4.836 5.635h.203l.187-.079c2.315-.98 3.181-3.202 3.812-4.824.427-1.095.807-2.21 1.174-3.288.928-2.723 1.804-5.295 3.383-7.604 1.197-1.751 4.425-5.563 8.556-3.101 2.755 1.641 3.619 4.952 4.455 8.154.238.914.485 1.859.771 2.741 1.211 3.729 2.784 7.574 5.268 7.829 1.294.128 2.515-.751 3.588-2.63 1.96-3.435 2.801-7.261 3.613-10.961l.285-1.293c.32-1.43.676-2.905 1.039-4.413 2.043-8.489 4.359-18.111 2.147-26.472zm-4.092 26.004a283.52 283.52 0 0 0-1.046 4.444l-.288 1.301c-.78 3.555-1.587 7.229-3.396 10.398-.608 1.066-1.21 1.655-1.647 1.633-.458-.047-1.722-.771-3.569-6.457-.269-.826-.497-1.702-.738-2.629-.895-3.425-1.907-7.307-5.367-9.368-3.817-2.271-8.12-.857-11.229 3.69-1.733 2.535-2.695 5.358-3.625 8.088-.361 1.059-.733 2.153-1.145 3.207-.64 1.642-1.238 2.961-2.512 3.608-1.133-.283-2.344-3.229-2.813-4.373l-.122-.297c-3.392-8.188-4.729-17.386-6.023-26.28-.183-1.255-.364-2.503-.55-3.74-1.01-6.71 1.05-14.57 7.928-17.009 1.12-.396 2.389-.557 3.717-.557 3.058 0 6.428.849 9.024 1.611 6.423 3.649 6.423 5.228 6.397 11.753l-.003 1.32a1 1 0 1 0 2 0l.003-1.313c.022-5.719.014-8.413-4.556-11.691h.001c.273-.106.607-.236 1.041-.387.146-.051.291-.112.438-.176l.149-.064c4.951-2.047 10.392-1.8 14.199.648 3.108 1.998 5.2 4.536 5.891 7.147 2.083 7.873-.171 17.234-2.159 25.493z"/><path d="M20.966 11.005H19.38l-2.414-2.414V7.005a1 1 0 1 0-2 0v1.586l-2.414 2.414h-1.586a1 1 0 1 0 0 2h1.586l2.414 2.414v1.586a1 1 0 1 0 2 0v-1.586l2.414-2.414h1.586a1 1 0 1 0 0-2zm-5 2.586l-1.586-1.586 1.586-1.586 1.586 1.586-1.586 1.586zM46.966 29.005H45.38l-2.414-2.414v-1.586a1 1 0 1 0-2 0v1.586l-2.414 2.414h-1.586a1 1 0 1 0 0 2h1.586l2.414 2.414v1.586a1 1 0 1 0 2 0v-1.586l2.414-2.414h1.586a1 1 0 1 0 0-2zm-5 2.586l-1.586-1.586 1.586-1.586 1.586 1.586-1.586 1.586z"/></g></svg>
          </div>
          <h2>CARDIOLOGY CARE</h2>
          <p>
            A cardiology is a physician and
            specializing cardiology
          </p>
        </div>
        <div class="swiper-nav--item">
          <div class="svg">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><g fill="#FFF"><path d="M55.267 27h-2.244c-4.419 0-8.469 2.493-10.513 6.33a14.023 14.023 0 0 0-1.832-2.295c-.948-.956-1.971-2.399-3.054-3.928C35.147 23.613 32.586 20 29.501 20c-2.998 0-5.467 3.746-7.855 7.369-1.069 1.622-2.079 3.154-3.018 4.159-.626.67-1.158 1.406-1.64 2.169A14.604 14.604 0 0 0 4.714 27c-1.2 0-2.293.557-3 1.526a3.674 3.674 0 0 0-.532 3.324l8.251 25.577A3.7 3.7 0 0 0 12.965 60h34.051a3.7 3.7 0 0 0 3.532-2.572l8.251-25.577a3.676 3.676 0 0 0-.532-3.324 3.675 3.675 0 0 0-3-1.527zM20.09 32.894c1.054-1.129 2.109-2.729 3.226-4.424 1.9-2.883 4.265-6.47 6.185-6.47 2.051 0 4.41 3.328 6.491 6.265 1.132 1.597 2.201 3.105 3.265 4.179a12.18 12.18 0 0 1 2.524 3.77c.063.147.117.297.174.445.115.298.219.601.312.907.041.137.087.272.123.41.108.41.194.825.26 1.245.022.141.038.282.055.424.045.358.075.718.089 1.08.004.104.014.208.016.313a12.55 12.55 0 0 1-.098 1.696c-.057.466-.13.932-.241 1.394-.212.881-.417 1.743-.605 2.581l-.184.832c-.513 2.333-.997 4.537-2.119 6.503-.427.748-.694.851-.69.855-.084-.02-.807-.295-1.962-3.854-.17-.525-.315-1.084-.464-1.65-.586-2.245-1.25-4.79-3.578-6.176-2.606-1.552-5.528-.613-7.625 2.453-1.129 1.652-1.749 3.47-2.347 5.228-.23.677-.461 1.354-.722 2.024-.364.934-.681 1.646-1.287 2.024-.228-.147-.671-.689-1.421-2.516l-.078-.19c-.962-2.324-1.75-5.001-2.408-8.184a12.685 12.685 0 0 1-.195-1.348c-.01-.104-.02-.208-.027-.312a12.666 12.666 0 0 1-.032-1.32c.002-.065.01-.129.014-.194.017-.364.055-.726.103-1.085.021-.154.042-.307.069-.46a12.358 12.358 0 0 1 .445-1.762 12.57 12.57 0 0 1 .459-1.211 12.408 12.408 0 0 1 2.273-3.472zm36.806-1.657l-8.251 25.577A1.708 1.708 0 0 1 47.016 58H12.965c-.746 0-1.4-.477-1.629-1.186L3.085 31.237c-.17-.527-.08-1.086.246-1.533S4.161 29 4.714 29a12.61 12.61 0 0 1 11.023 6.492c.034.123.07.245.106.366-.081.196-.14.4-.213.599a14.196 14.196 0 0 0-.431 1.352c-.113.431-.209.865-.282 1.303-.021.127-.034.254-.051.382-.059.423-.1.848-.121 1.275-.005.095-.014.188-.017.283-.016.507-.001 1.014.036 1.522.008.107.019.213.029.32.05.524.119 1.048.227 1.57.684 3.308 1.508 6.103 2.52 8.545l.076.185C18.201 54.617 19.18 57 21.027 57h.203l.187-.079c1.612-.683 2.219-2.239 2.621-3.27.271-.697.512-1.402.752-2.105.581-1.706 1.13-3.317 2.105-4.744.111-.162.254-.355.42-.558.892-1.087 2.544-2.49 4.53-1.305 1.623.966 2.13 2.907 2.666 4.962.158.605.314 1.202.497 1.763.806 2.481 1.87 5.043 3.658 5.226.987.104 1.874-.521 2.634-1.854 1.272-2.23 1.813-4.688 2.335-7.065l.182-.823c.186-.828.39-1.681.599-2.552.083-.345.149-.692.206-1.04.016-.096.026-.192.04-.287a15.015 15.015 0 0 0 .114-1.085c.019-.278.029-.555.031-.833.001-.075.003-.15.003-.226a14.701 14.701 0 0 0-.173-2.171c-.005-.034-.013-.068-.018-.103a15.089 15.089 0 0 0-.187-.947c-.016-.071-.034-.141-.051-.211a14.767 14.767 0 0 0-.245-.876l-.051-.166a14.88 14.88 0 0 0-.373-1.023l.061-.272C45.232 31.551 48.941 29 53.023 29h2.244c.553 0 1.058.257 1.383.704s.416 1.006.246 1.533z"/><path d="M58.001 10.242H43.87c-6.586 0-12.778-2.564-17.434-7.221L23.708.293l-.005-.004a1.017 1.017 0 0 0-.284-.192c-.029-.013-.058-.021-.088-.032a.975.975 0 0 0-.277-.056C23.035.01 23.019 0 23.001 0h-12a1 1 0 0 0-1 1v12c0 3.519 2.613 6.432 6 6.92v3.358c-.595.347-1 .985-1 1.722 0 1.103.897 2 2 2s2-.897 2-2c0-.737-.405-1.375-1-1.722V19.92c3.387-.488 6-3.401 6-6.92v-1.403a26.727 26.727 0 0 0 17.605 6.645h16.395a1 1 0 0 0 1-1v-6a1 1 0 0 0-1-1zm-8.382 6l-2-4h3.303l2.337 4h-3.64zm-4.236-4l2 4h-3.764l-2.052-4.104c.762.065 1.53.104 2.303.104h1.513zM22.001 2v1h-2a1 1 0 1 0 0 2h2v6h-10V5h2a1 1 0 1 0 0-2h-2V2h10zm-5 16c-2.757 0-5-2.243-5-5h10c0 2.757-2.243 5-5 5zm7-14.586l1.021 1.021a26.45 26.45 0 0 0 14.151 7.389l2.206 4.412c-6.54-.061-12.857-2.741-17.379-7.387V3.414zm33 12.828h-1.426l-2.337-4h3.763v4z"/></g></svg>
          </div>
          <h2>UROlogy CARE</h2>
          <p>
            A urology is a physician and
            specialize in urology
          </p>
        </div>
      </div>
    </div>
  </section>

  <section data-aos="fade-up" id="wecare" class="section section-home section-wecare">
    <div class="container">
      <h2>WE CARE</h2>
      <div class="svg">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14"><defs><filter id="Filter_0"><feFlood flood-color="#D14E45" flood-opacity="1" result="floodOut"/><feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/><feBlend in="compOut" in2="SourceGraphic"/></filter></defs><g filter="url(#Filter_0)"><path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.998h32.312V8.67H45.685V6.998zM31.178 7.499l2.224-3.851 1.447.836-2.223 3.851-1.448-.836z"/><path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.79 1.448-.835z"/><path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.694l2.307-8.61 1.615.433-2.307 8.61-1.615-.433z"/><path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.739l1.615-.432 3.01 11.233-1.615.432z"/><image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGDhMUHjIGZaUAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/><path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.998h32.312V8.67H.003V6.998z"/></g></svg>
      </div>
      <p class="section--subheading">
        Medical Services is the term used to describe the range of healthcare that is provided by General
        Practitioners as part of the National Health Service in our Hospitals.
      </p>

      <div class="card-deck">
        <div class="card">
          <img class="card-img-top" src="@asset('images/demo-1.jpg')" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <small class="date">20th August 2016, Medicare</small>
            <p class="card-text card-excerpt">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
            <p class="card-text card-readmore"><a href="#">Read more <i class="fas fa-long-arrow-alt-right"></i></a></p>
          </div>
        </div>
        <div class="card">
          <img class="card-img-top" src="@asset('images/demo-1.jpg')" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <small class="date">20th August 2016, Medicare</small>
            <p class="card-text card-excerpt">This card has supporting text below as a natural lead-in to additional content.</p>
            <p class="card-text card-readmore"><a href="#">Read more <i class="fas fa-long-arrow-alt-right"></i></a></p>
          </div>
        </div>
        <div class="card">
          <img class="card-img-top" src="@asset('images/demo-1.jpg')" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <small class="date">20th August 2016, Medicare</small>
            <p class="card-text card-excerpt">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
            <p class="card-text card-readmore"><a href="#">Read more <i class="fas fa-long-arrow-alt-right"></i></a></p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section data-aos="fade-up" id="features" class="section section-home section-features">
    <div class="container">

      <h2>FEATURES</h2>
      <div class="svg">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="79" height="14"><defs><filter id="a"><feFlood flood-color="#FFF" flood-opacity="1" result="floodOut"/><feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/><feBlend in="compOut" in2="SourceGraphic"/></filter></defs><g filter="url(#a)"><path fill-rule="evenodd" fill="#29B6F6" d="M46.624 6.922h32.298v1.669H46.624V6.922zM32.123 7.422l2.223-3.845 1.447.834-2.223 3.846-1.447-.835z"/><path fill-rule="evenodd" fill="#29B6F6" d="M35.653 2.78l3.342 5.781-1.447.834-3.341-5.781 1.446-.834z"/><path fill-rule="evenodd" fill="#29B6F6" d="M37.982 9.614l2.306-8.597 1.614.431-2.306 8.598-1.614-.432z"/><path fill-rule="evenodd" fill="#29B6F6" d="M43.03 11.889L40.021.672 41.635.24l3.008 11.217-1.613.432z"/><image x="43" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAQAAACb+P2wAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiBg4TEwHwT/6XAAAAT0lEQVQI1wXBIRXCAABAwc94vIdiigTLsAhIOlCAHJRALQMR0CRATBMAgzjukuxNtsng7GlORi9fp+xccVMmH28H5e7nIg2tPVqqsnGU9AeKwi6gOaYM8gAAAABJRU5ErkJggg=="/><path fill-rule="evenodd" fill="#29B6F6" d="M.961 6.922h32.298v1.669H.961V6.922z"/></g></svg>
      </div>
      <p class="section--subheading pb-4">
        Medical Services is the term used to describe the range of healthcare that is provided by General
        Practitioners as part of the National Health Service in our Hospitals.
      </p>

      <div class="row">
        <div class="col">
          <div class="card border-0 bg-transparent text-center">
            <div class="card-body">
              <div class="card-header bg-transparent border-0">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="84" height="83"><path stroke="#FFF" stroke-width="2" fill="none" d="M41.338.998c22.101 0 40.018 17.909 40.018 40 0 22.092-17.917 40.001-40.018 40.001S1.32 63.09 1.32 40.998c0-22.091 17.917-40 40.018-40z"/><image x="25" y="22" width="32" height="30" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAeCAQAAADnA9SiAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiBg4SKAS9VY8XAAACJUlEQVQ4y52US0iUYRSGnxkdHS9IJpZZdJFy04UwspIudMFF7kKDSFpEUa0C94Kt3LQI2hS0aBNRUKtAoyCKJCICaVEt3GSmRkaRNIyjztNCZ5y7Y+/PvzjnP+f5zn++831Iwee0bzxVKCJIYbWyn1Yq8wcsB5gFLBSQCljNHmozvgcW31RV00JdLsAV3tGXgYgDUebSfL285yrhRSulId1+V3sMpfi2esZNaW3r8Y96PmGn97TXqDE7k3aF9a6zwXDS0+mY2mdVbgDeVPsNG/CYd/3khD/85ogP7LBCvKHesX4pIxOwynZrbfaRMXXOiHMm9NSdNnrcNakZuYbjhCPqT2/b4T1nXNKk7ZnR2ekH/Ki+dp/Ya8R0jXmoMCDsoDpgk9jthNkatCkdELLMYNJx2b+Ou1es84W5dSF1wVKGWMsoo7zkIb85SSXXGAbaac4zvV08Y5IujrKZFvySJHe53c9GPSjidWN5KvjqNs86tWCUspv17OII5QzRRiMfmAKCNBPKU0E9G3jFfcp5y3B6Cy856xM3itV5O6B6rtB9MI9AkJLiznA2IMDyshDA5CEtSrkAAnPEiwOUZtgR2rjFH6rY8T+AMHHq6Vg2qywfYIBpwhkXWPaS8nzJDAhQRQMhJMZMUc0LEyIAjDO9UMFh+tnCTHF/nVLJRR4vAMqppWZF6QuqSGzjfLG7nqF4AlBS1PxlK5jYhQi/qCG6ouQy5onAP16gISiM44jhAAAAAElFTkSuQmCC"/></svg>
              </div>
              <h5 class="card-title">Card title</h5>
              <p class="card-text pb-4">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
              <a class="readmore" href="javascript:">Read more</a>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card border-0 bg-transparent text-center">
            <div class="card-body">
              <div class="card-header bg-transparent border-0">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="84" height="83"><path stroke="#FFF" stroke-width="2" fill="none" d="M41.515.998c22.101 0 40.018 17.909 40.018 40 0 22.092-17.917 40.001-40.018 40.001-22.102 0-40.018-17.909-40.018-40.001 0-22.091 17.916-40 40.018-40z"/><image x="28" y="26" width="28" height="30" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAeCAQAAACVzLYUAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiBg4TAA2YFvMuAAAB/ElEQVQ4y53Uz2+MYRAH8M++3e0qXVQRHGki0YgEiUg4OFQicSBxEHFxcBBJ/wSJpHd34u7g0ItGb62IyNKeOLSpqBDEzyCstnbHYd9db7XVtd+5zDt5vs/MfN+ZR0jtdDyNNzHY/F5s52ImXsTFP5GGszPuRkTEkzi6DG1PjEVERDn2N2KJOtbpBhv1WIpuJbDexkYob4eD8vpsBWsdUbNuEa1ir16wwTEleZOe52LUcf+LScO5iP+mwdukLRqFdom1donyYMFHP6x+SU3RZkUQERHP4mSItauaOBQPIyLiXT696Sd+tFBhxa+6Uy8vJ9dia7lGQ9m+Boy4p+yqxCkPjCm7hEGPjRl3dqk4dexyArzTZbfD4JGifgfAfbc0xyWbcd4CqKqZb+pYVUv9uWzGLLGoADokqeQkOppnulYq9aEhXQrKvht3TVWnUXPumPMTI2REjIiImRhYYfP/tn3xoP4fs6WeMe21z64ruOCDl764giFfvfLaZf6Iky21R58ctivoTVd3q05blJSwbSVxqulU1IRqoxO1pqoLKxELqaoJOjOT0jjTmSVmSx113hqJaRXD3qPDhHk3TVhQ8yirapY4a7bpT5lq+mVlS5Ck3VW1hoYOacZu+31LX9Z/oaLfprT5Nl+5T0lL67sMMe+Gi+a0njdnjWG3fwOvTvgO2yfF3QAAAABJRU5ErkJggg=="/></svg>
              </div>
              <h5 class="card-title">Card title</h5>
              <p class="card-text pb-4">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
              <a class="readmore" href="javascript:">Read more</a>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card border-0 bg-transparent text-center">
            <div class="card-body">
              <div class="card-header bg-transparent border-0">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="84" height="83"><path stroke="#FFF" stroke-width="2" fill="none" d="M41.692.998c22.101 0 40.017 17.909 40.017 40 0 22.092-17.916 40.001-40.017 40.001-22.102 0-40.018-17.909-40.018-40.001 0-22.091 17.916-40 40.018-40z"/><image x="26" y="29" width="33" height="30" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAeCAQAAAAIwb+cAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiBg4TACzUf+NwAAACaklEQVQ4y6WU3WuOcRjHP3u2ad6WsqzmhJK1zUSShCJZZEeokeRATpASCX+A4kQ7cUKUlyJ5yQ7QUF4XhSw2bMZmb96aPV6XbR8HuzfP89z3Myvf38l9/b7f63tf9/W77l+GBBhDGWuYwQR+0MgNztNKIgqoYBlTGE0XT7nENX4FjANrvg9Mxgd3GQvYXHf6MYnt966LBljEmOuMG0afpxwnTvKYUXjvpkGLcntMh2NmeTAt2+ZawUJrTI/3XrdrGP6xZbjd/0GPNTHK+R+MojjGrBFJq1lGdSTTy4jKbbNC3BjJfY6NqIZazgK1vIsiY/T/0+ATFwFooSraojW0108rLxPiOk4C8Jnr0RbPQnt1LGY1HUHUxQV+EiMLeMGjkDozxp2UrV9c5DUNHArieo4Dc9gDNHMlVPE7XOTXpA4/dJKIxT5Su90tjrfSJvPEFSnn8cZ9GY6mkDx6h3zbeRU8beAETyinnblcRbZxhrEUkUvfUBu+0DD4syevPKeLhd70qJjjPlWPRGrJECCHfHoxmLYC9pLNKrKZRgZ1lHKOQuAVW3hCzlAF0MV3giunwNNqt3HjdvtDfePy4C1Z7hj68t+BJu5Xe6xy3OB9gTjb+pRGXQ6YEmsjB/u5s/7eWgNrgS1Jko9uFjPdGmnQ6JLBzMTGLLUzSXbLTKd6L8LgrSv/5iV3d6nNCcK4pz0cYdBkWWJW6hHNC/UkFS9cmJwTPuci7w9jUGNJakbUsEz2bGR6n+fND+ujp3OC+0MG3zxoZpQ62gKzrLAjwaDN9WmUaS0QS70TGNx2ZnrdcBY40QN2Wun44VR/ANHiPOkeUtesAAAAAElFTkSuQmCC"/></svg>
              </div>
              <h5 class="card-title">Card title</h5>
              <p class="card-text pb-4">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
              <a class="readmore" href="javascript:">Read more</a>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

  <section data-aos="fade-up" class="section section-home section-map">
    <button class="show_map btn btn-action text-white" type="button">Show map</button>
    <div id="map" class="map" data-lat="40.7056258" data-lng="-73.97968"></div>
    <div class="container">
      <h2>THUN</h2>
      <div class="svg">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14"><defs><filter id="Filter_0"><feFlood flood-color="#D14E45" flood-opacity="1" result="floodOut"/><feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/><feBlend in="compOut" in2="SourceGraphic"/></filter></defs><g filter="url(#Filter_0)"><path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.999h32.312V8.67H45.685V6.999zM31.178 7.5l2.224-3.851 1.447.836-2.223 3.85-1.448-.835z"/><path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.789 1.448-.836z"/><path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.695l2.307-8.61 1.615.433-2.307 8.609-1.615-.432z"/><path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.74l1.615-.433 3.01 11.233-1.615.432z"/><image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGDw03Ajaqp5QAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/><path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.999h32.312V8.67H.003V6.999z"/></g></svg>
      </div>
      <div class="address">
        <span>Zahnarztpraxis Thun Zentrum</span>, Aarestrasse 10, 3600 Thun
      </div>

      <div class="card-group">
        <div class="card bg-transparent">
          <div class="card-body">
            <div class="svg">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="38" height="40"><image width="38" height="40" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAoCAMAAACl6XjsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABgFBMVEXRTkX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX////qQWzVAAAAfnRSTlMAABOmpBJE2NZABH/7+n4mul3rOKm7WVTz7k1hEVKhVQZPHENaCoAJkpCvqgEQ3lv3qwwFnznoM77EISK8MukIjeE/7fZiK8mIApetZPHNKc7qSND5ZvSJByfC/uW/JBjnh9w7e92EHbkbvbcP45WW4kV3+Gpt78Ylx5Oop54NOn/dAAAAAWJLR0QB/wIt3gAAAAd0SU1FB+IGDw4JNyOaxpMAAAIUSURBVDjLjdPpXxJBGAfwB4xIkBRFZRHkTEwyZTutCBQ88YIUL9JKKys71LLyeP725pnF3dll1vy92M/M83zZZWd2AKxxttxwCVMHT5O66Ua81fof5vG2IYvvtucq1t6BjXS02zN/J+rp9DezrgClG03p5sUugfWgbXoE1osYVCQJIvYKLITYF5akDzEkMC9iBCSJIHp15uxn/yEKsXiUJSGyKGv0OzWWdCNnLr6yKStDd5KxOwO8ewXDtoE0DDZe3PahlLswpDNJLtkQZHR2b1hci5jIMgIbvi+ubFzOQuAybdJIY9l1NqrQKKs+eJgR8ogpNUsdZZSzx0+e0mzsmfUFno9R/UXuJWdKPlag+fiEWU2MU7UQyysaYyckVWSV0qSoJkusVEw5HC6DwdQ0/XRm9hLNztB8egrMDNQ5qpfDmgqXaTangpUBzC+wzuISDZcW2XBhXjsLFga5ZdasVAGqFTZYzoGcQfoVa6+srq7QbdNgw9KFkrELtbV1OdvYZN2tOpn6Fru83paxCP2h4MhOADGw46+xyZt4E/O8pbu822XDvT122X1Pt/7gMbOP+3z7PhmbkPhMlS8Jgx1A9SvVvq2Lm5X/TrUfh44DjVXUIzosxz+tX8ivYzovR2qFsxP+wHqy+Sgc8tfePzG+3t/bsiPz56/pI/edgk1OfQKrnZ1npTk/q2msjNdIGS6K18jFPzSkK92tWo6iAAAAAElFTkSuQmCC"/></svg>
            </div>
            <p class="card-text"><a href="mailto:info@zahnarzt-thun-zentrum.ch">info@zahnarzt-thun-zentrum.ch</a></p>
          </div>
        </div>
        <div class="card bg-transparent">
          <div class="card-body">
            <div class="svg">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="40"><image width="40" height="40" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABgFBMVEXRTkX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX////qQWzVAAAAfnRSTlMAAA4xWn+iuszb4+9+WSJsteY2rPY1evxwKPefuaGjnRXw/mFU8Q1Tkg+RwBAZuwq/3/SPY/PEBKaCBcbyd+XTtIkWgQEXbdTkdgIe6doDFMgJvQivzV/toJQwh1hJhUDPgBFdS0hQTzg3IUcsK1vOgxMSdHJeuNf9B977HGZr9Bv3AAAAAWJLR0QB/wIt3gAAAAd0SU1FB+IGDw4LElqocFYAAAJBSURBVDjLjZT9X9JQFMYPk0BYOpcGKpIwk6I0Qw2kqGmssBQyU0zLLIxebKS9+K73b/fc7d65jRU8v9xzz/nus93znDsALqHDfyUQ7AyFRVQ41BkMXO3qEMDHxKhuqUcmHpJ7pG4beK23j/xTfb3XORiJkv8qGjHBftJS/RQcGGwNDg4gGLPeMBS/4VB8yPqmYR8kkixWRqBJIworJhNwkz80mmoGU6O8egtu81BM01KaM+ZO5NU7cNcCxwDG74mBCUpMBMTgOMCYBd6HDA8nEZzCdZqC0xhMITjJqxl4wMNsDmAG1zwF8xjMADx8xKsFeCw/UbOzw3PSUzBbVdRQRQximHj2fC42my3Nyy9waoSX/AALIawvqqWSuohBeYHmqM+VlOCzNyNRcvqhvmKgbcyMpi25nVtKeYGvl5s9Xn7TDOZWWHE1X63mV9lmJecGtbhZWfObe/+auX+rucB1xm3wxAYj153gO/aq99TgzU1qtJ+lPtjBCP8+jLfQ1MwWBh9ZMnIJSvyUnwBqxm2UawBFnpU4uG21YxvgsxnVHWkT/BJtBUa/UvCb7U57vxov13cfVOzGeR7GMLMCO47bb7bnh4BLlz0f3QHdYW5zw5l0aChO0q8Zjv50ckrDDRKyu1et7u26kgjqpC3p8CvZDif+BvjTDviXOrN/cHiEcQgvlVsqvW1Hhwf7bChq+ENQdM1DOh5VrFnTky7ggw3wUAMLhbQFHp8QcnrmBZ6dEnJyfDm49fL8OXjqPFyuG4N7AVUex3uo9AtAAAAAAElFTkSuQmCC"/></svg>
            </div>
            <p class="card-text"><a href="tel:+410332222209">Termine: +41 (0) 33 222 22 09</a></p>
          </div>
        </div>
        <div class="card bg-transparent">
          <div class="card-body">
            <div class="svg">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="40"><image width="40" height="40" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABgFBMVEXRTkX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX////qQWzVAAAAfnRSTlMAAA4xWn+iuszb4+9+WSJsteY2rPY1evxwKPefuaGjnRXw/mFU8Q1Tkg+RwBAZuwq/3/SPY/PEBKaCBcbyd+XTtIkWgQEXbdTkdgIe6doDFMgJvQivzV/toJQwh1hJhUDPgBFdS0hQTzg3IUcsK1vOgxMSdHJeuNf9B977HGZr9Bv3AAAAAWJLR0QB/wIt3gAAAAd0SU1FB+IGDw4MCOiLH+sAAAJBSURBVDjLjZT9X9JQFMYPk0BYOpcGKpIwk6I0Qw2kqGmssBQyU0zLLIxebKS9+K73b/fc7d65jRU8v9xzz/nus93znDsALqHDfyUQ7AyFRVQ41BkMXO3qEMDHxKhuqUcmHpJ7pG4beK23j/xTfb3XORiJkv8qGjHBftJS/RQcGGwNDg4gGLPeMBS/4VB8yPqmYR8kkixWRqBJIworJhNwkz80mmoGU6O8egtu81BM01KaM+ZO5NU7cNcCxwDG74mBCUpMBMTgOMCYBd6HDA8nEZzCdZqC0xhMITjJqxl4wMNsDmAG1zwF8xjMADx8xKsFeCw/UbOzw3PSUzBbVdRQRQximHj2fC42my3Nyy9waoSX/AALIawvqqWSuohBeYHmqM+VlOCzNyNRcvqhvmKgbcyMpi25nVtKeYGvl5s9Xn7TDOZWWHE1X63mV9lmJecGtbhZWfObe/+auX+rucB1xm3wxAYj153gO/aq99TgzU1qtJ+lPtjBCP8+jLfQ1MwWBh9ZMnIJSvyUnwBqxm2UawBFnpU4uG21YxvgsxnVHWkT/BJtBUa/UvCb7U57vxov13cfVOzGeR7GMLMCO47bb7bnh4BLlz0f3QHdYW5zw5l0aChO0q8Zjv50ckrDDRKyu1et7u26kgjqpC3p8CvZDif+BvjTDviXOrN/cHiEcQgvlVsqvW1Hhwf7bChq+ENQdM1DOh5VrFnTky7ggw3wUAMLhbQFHp8QcnrmBZ6dEnJyfDm49fL8OXjqPFyuG4N7AVUex3uo9AtAAAAAAElFTkSuQmCC"/></svg>
            </div>
            <p class="card-text"><a href="tel:+410332222209">Notfälle: +41 (0) 33 222 22 09</a></p>
          </div>
        </div>
      </div>

    </div>
  </section>

  <section data-aos="fade-up" class="section section-home section-contact">
    <div class="container">

      <h2>Get in touch</h2>
      <div class="svg">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14"><defs><filter id="a"><feFlood flood-color="#fff" flood-opacity="1" result="floodOut"/><feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/><feBlend in="compOut" in2="SourceGraphic"/></filter></defs><g filter="url(#a)"><path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.998h32.312V8.67H45.685V6.998zM31.178 7.499l2.224-3.851 1.447.836-2.223 3.851-1.448-.836z"/><path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.79 1.448-.835z"/><path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.694l2.307-8.61 1.615.433-2.307 8.61-1.615-.433z"/><path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.739l1.615-.432 3.01 11.233-1.615.432z"/><image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGDQAoLxz+rqcAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/><path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.998h32.312V8.67H.003V6.998z"/></g></svg>
      </div>
      <p class="section--subheading">
        Asunt  loreute irure dolor in reprehenderit in voluptate cillum luptate dolor in reprehendercil
        dolore eu  pariatur. Excepteur sint luptate cil dolor in reprehenderit in occaecat
      </p>

      <form method="POST" action="" class="contact-form" id="contact_form">
        <div class="form-row">
          <div class="form-group col">
            <input type="text" name="firstname" class="form-control" placeholder="Your name">
          </div>
          <div class="form-group col">
            <input type="text" name="email" class="form-control" placeholder="Email">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-lg-12">
            <textarea name="message" rows="10" class="form-control" placeholder="Message..."></textarea>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-lg-12 text-center pt-4">
            <button class="btn btn-outline-light" type="submit">Send</button>
          </div>
        </div>
      </form>
    </div>
  </section>
@endsection
