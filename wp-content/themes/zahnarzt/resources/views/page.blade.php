@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <div class="main">
      @include('partials.page-header')
      @include('partials.content-' . get_post_field( 'post_name', get_post() ))
    </div>
  @endwhile
@endsection
