<section data-aos="fade-up" class="section section-home section-map">
  <button class="show_map btn btn-action text-white" type="button">Show map</button>
  <div id="map" class="map" data-lat="40.7056258" data-lng="-73.97968"></div>
  <div class="container">
    <h2>THUN</h2>
    <div class="svg">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14"><defs><filter id="Filter_0"><feFlood flood-color="#D14E45" flood-opacity="1" result="floodOut"/><feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/><feBlend in="compOut" in2="SourceGraphic"/></filter></defs><g filter="url(#Filter_0)"><path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.999h32.312V8.67H45.685V6.999zM31.178 7.5l2.224-3.851 1.447.836-2.223 3.85-1.448-.835z"/><path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.789 1.448-.836z"/><path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.695l2.307-8.61 1.615.433-2.307 8.609-1.615-.432z"/><path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.74l1.615-.433 3.01 11.233-1.615.432z"/><image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGDw03Ajaqp5QAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/><path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.999h32.312V8.67H.003V6.999z"/></g></svg>
    </div>
    <div class="address">
      <span>Zahnarztpraxis Thun Zentrum</span>, Aarestrasse 10, 3600 Thun
    </div>

    <div class="card-group">
      <div class="card bg-transparent">
        <div class="card-body">
          <div class="svg">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="38" height="40"><image width="38" height="40" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAoCAMAAACl6XjsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABgFBMVEXRTkX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX////qQWzVAAAAfnRSTlMAABOmpBJE2NZABH/7+n4mul3rOKm7WVTz7k1hEVKhVQZPHENaCoAJkpCvqgEQ3lv3qwwFnznoM77EISK8MukIjeE/7fZiK8mIApetZPHNKc7qSND5ZvSJByfC/uW/JBjnh9w7e92EHbkbvbcP45WW4kV3+Gpt78Ylx5Oop54NOn/dAAAAAWJLR0QB/wIt3gAAAAd0SU1FB+IGDw4JNyOaxpMAAAIUSURBVDjLjdPpXxJBGAfwB4xIkBRFZRHkTEwyZTutCBQ88YIUL9JKKys71LLyeP725pnF3dll1vy92M/M83zZZWd2AKxxttxwCVMHT5O66Ua81fof5vG2IYvvtucq1t6BjXS02zN/J+rp9DezrgClG03p5sUugfWgbXoE1osYVCQJIvYKLITYF5akDzEkMC9iBCSJIHp15uxn/yEKsXiUJSGyKGv0OzWWdCNnLr6yKStDd5KxOwO8ewXDtoE0DDZe3PahlLswpDNJLtkQZHR2b1hci5jIMgIbvi+ubFzOQuAybdJIY9l1NqrQKKs+eJgR8ogpNUsdZZSzx0+e0mzsmfUFno9R/UXuJWdKPlag+fiEWU2MU7UQyysaYyckVWSV0qSoJkusVEw5HC6DwdQ0/XRm9hLNztB8egrMDNQ5qpfDmgqXaTangpUBzC+wzuISDZcW2XBhXjsLFga5ZdasVAGqFTZYzoGcQfoVa6+srq7QbdNgw9KFkrELtbV1OdvYZN2tOpn6Fru83paxCP2h4MhOADGw46+xyZt4E/O8pbu822XDvT122X1Pt/7gMbOP+3z7PhmbkPhMlS8Jgx1A9SvVvq2Lm5X/TrUfh44DjVXUIzosxz+tX8ivYzovR2qFsxP+wHqy+Sgc8tfePzG+3t/bsiPz56/pI/edgk1OfQKrnZ1npTk/q2msjNdIGS6K18jFPzSkK92tWo6iAAAAAElFTkSuQmCC"/></svg>
          </div>
          <p class="card-text"><a href="mailto:info@zahnarzt-thun-zentrum.ch">info@zahnarzt-thun-zentrum.ch</a></p>
        </div>
      </div>
      <div class="card bg-transparent">
        <div class="card-body">
          <div class="svg">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="40"><image width="40" height="40" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABgFBMVEXRTkX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX////qQWzVAAAAfnRSTlMAAA4xWn+iuszb4+9+WSJsteY2rPY1evxwKPefuaGjnRXw/mFU8Q1Tkg+RwBAZuwq/3/SPY/PEBKaCBcbyd+XTtIkWgQEXbdTkdgIe6doDFMgJvQivzV/toJQwh1hJhUDPgBFdS0hQTzg3IUcsK1vOgxMSdHJeuNf9B977HGZr9Bv3AAAAAWJLR0QB/wIt3gAAAAd0SU1FB+IGDw4LElqocFYAAAJBSURBVDjLjZT9X9JQFMYPk0BYOpcGKpIwk6I0Qw2kqGmssBQyU0zLLIxebKS9+K73b/fc7d65jRU8v9xzz/nus93znDsALqHDfyUQ7AyFRVQ41BkMXO3qEMDHxKhuqUcmHpJ7pG4beK23j/xTfb3XORiJkv8qGjHBftJS/RQcGGwNDg4gGLPeMBS/4VB8yPqmYR8kkixWRqBJIworJhNwkz80mmoGU6O8egtu81BM01KaM+ZO5NU7cNcCxwDG74mBCUpMBMTgOMCYBd6HDA8nEZzCdZqC0xhMITjJqxl4wMNsDmAG1zwF8xjMADx8xKsFeCw/UbOzw3PSUzBbVdRQRQximHj2fC42my3Nyy9waoSX/AALIawvqqWSuohBeYHmqM+VlOCzNyNRcvqhvmKgbcyMpi25nVtKeYGvl5s9Xn7TDOZWWHE1X63mV9lmJecGtbhZWfObe/+auX+rucB1xm3wxAYj153gO/aq99TgzU1qtJ+lPtjBCP8+jLfQ1MwWBh9ZMnIJSvyUnwBqxm2UawBFnpU4uG21YxvgsxnVHWkT/BJtBUa/UvCb7U57vxov13cfVOzGeR7GMLMCO47bb7bnh4BLlz0f3QHdYW5zw5l0aChO0q8Zjv50ckrDDRKyu1et7u26kgjqpC3p8CvZDif+BvjTDviXOrN/cHiEcQgvlVsqvW1Hhwf7bChq+ENQdM1DOh5VrFnTky7ggw3wUAMLhbQFHp8QcnrmBZ6dEnJyfDm49fL8OXjqPFyuG4N7AVUex3uo9AtAAAAAAElFTkSuQmCC"/></svg>
          </div>
          <p class="card-text"><a href="tel:+410332222209">Termine: +41 (0) 33 222 22 09</a></p>
        </div>
      </div>
      <div class="card bg-transparent">
        <div class="card-body">
          <div class="svg">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="40"><image width="40" height="40" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABgFBMVEXRTkX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX////qQWzVAAAAfnRSTlMAAA4xWn+iuszb4+9+WSJsteY2rPY1evxwKPefuaGjnRXw/mFU8Q1Tkg+RwBAZuwq/3/SPY/PEBKaCBcbyd+XTtIkWgQEXbdTkdgIe6doDFMgJvQivzV/toJQwh1hJhUDPgBFdS0hQTzg3IUcsK1vOgxMSdHJeuNf9B977HGZr9Bv3AAAAAWJLR0QB/wIt3gAAAAd0SU1FB+IGDw4MCOiLH+sAAAJBSURBVDjLjZT9X9JQFMYPk0BYOpcGKpIwk6I0Qw2kqGmssBQyU0zLLIxebKS9+K73b/fc7d65jRU8v9xzz/nus93znDsALqHDfyUQ7AyFRVQ41BkMXO3qEMDHxKhuqUcmHpJ7pG4beK23j/xTfb3XORiJkv8qGjHBftJS/RQcGGwNDg4gGLPeMBS/4VB8yPqmYR8kkixWRqBJIworJhNwkz80mmoGU6O8egtu81BM01KaM+ZO5NU7cNcCxwDG74mBCUpMBMTgOMCYBd6HDA8nEZzCdZqC0xhMITjJqxl4wMNsDmAG1zwF8xjMADx8xKsFeCw/UbOzw3PSUzBbVdRQRQximHj2fC42my3Nyy9waoSX/AALIawvqqWSuohBeYHmqM+VlOCzNyNRcvqhvmKgbcyMpi25nVtKeYGvl5s9Xn7TDOZWWHE1X63mV9lmJecGtbhZWfObe/+auX+rucB1xm3wxAYj153gO/aq99TgzU1qtJ+lPtjBCP8+jLfQ1MwWBh9ZMnIJSvyUnwBqxm2UawBFnpU4uG21YxvgsxnVHWkT/BJtBUa/UvCb7U57vxov13cfVOzGeR7GMLMCO47bb7bnh4BLlz0f3QHdYW5zw5l0aChO0q8Zjv50ckrDDRKyu1et7u26kgjqpC3p8CvZDif+BvjTDviXOrN/cHiEcQgvlVsqvW1Hhwf7bChq+ENQdM1DOh5VrFnTky7ggw3wUAMLhbQFHp8QcnrmBZ6dEnJyfDm49fL8OXjqPFyuG4N7AVUex3uo9AtAAAAAAElFTkSuQmCC"/></svg>
          </div>
          <p class="card-text"><a href="tel:+410332222209">Notfälle: +41 (0) 33 222 22 09</a></p>
        </div>
      </div>
    </div>

  </div>
</section>

<section data-aos="fade-up" class="section section-home section-contact">
  <div class="container">

    <h2>Get in touch</h2>
    <div class="svg">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14"><defs><filter id="a"><feFlood flood-color="#fff" flood-opacity="1" result="floodOut"/><feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/><feBlend in="compOut" in2="SourceGraphic"/></filter></defs><g filter="url(#a)"><path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.998h32.312V8.67H45.685V6.998zM31.178 7.499l2.224-3.851 1.447.836-2.223 3.851-1.448-.836z"/><path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.79 1.448-.835z"/><path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.694l2.307-8.61 1.615.433-2.307 8.61-1.615-.433z"/><path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.739l1.615-.432 3.01 11.233-1.615.432z"/><image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGDQAoLxz+rqcAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/><path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.998h32.312V8.67H.003V6.998z"/></g></svg>
    </div>
    <p class="section--subheading">
      Asunt  loreute irure dolor in reprehenderit in voluptate cillum luptate dolor in reprehendercil
      dolore eu  pariatur. Excepteur sint luptate cil dolor in reprehenderit in occaecat
    </p>

    <form method="POST" action="" class="contact-form" id="contact_form">
      <div class="form-row">
        <div class="form-group col">
          <input type="text" name="firstname" class="form-control" placeholder="Your name">
        </div>
        <div class="form-group col">
          <input type="text" name="email" class="form-control" placeholder="Email">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-lg-12">
          <textarea name="message" rows="10" class="form-control" placeholder="Message..."></textarea>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-lg-12 text-center pt-4">
          <button class="btn btn-outline-light" type="submit">Send</button>
        </div>
      </div>
    </form>
  </div>
</section>
