<header class="header">
  <div class="header--logo">
    <a href="{{ home_url('/') }}">
      <img src="@asset('images/logo.png')" alt="{{ get_bloginfo('name', 'display') }}">
    </a>
  </div>
  <div class="header--nav">
    <nav id="navigation" class="navigation">

      <div class="nav-header">
        <div class="nav-toggle"></div>
      </div>

      <div class="nav-menus-wrapper">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu([
            'theme_location' => 'primary_navigation',
            'menu_class' => 'nav-menu nav-menu-centered'
          ]) !!}
        @endif
      </div>

    </nav>
  </div>
</header>
