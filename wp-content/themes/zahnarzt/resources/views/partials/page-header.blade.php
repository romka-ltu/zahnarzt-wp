<div class="page-header" style="background-image:url({{ \App\headingImage() }})">
  <div class="container">
    <h1>
      {{ get_field('heading_text') }}
      <span>{!! App::title() !!}</span>
    </h1>
    @if(get_field('subheading_text'))
      <p>{{ get_field('subheading_text') }}</p>
    @endif
  </div>
</div>
