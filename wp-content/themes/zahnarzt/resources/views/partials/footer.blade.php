<div id="footer" class="footer">
  <div class="container">

    <div class="row">
      <div class="col-lg-12">
        <div class="footer--logo">
          <a href="{{ home_url('/') }}">
            <img src="@asset('images/logo-footer.png')" alt="">
          </a>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <h3>About us</h3>
        <p>
          Das Schützen gesunder Zähne ist das oberste Ziel und das
          Ersetzen von Zähnen die Kernkompetenz von Fachzahnärztin Ana Sosunova.
          Mit Präzision, Sorgfalt und Einfühlungsvermögen behandelt
          sie ihrer Patienten seit mehr als 10 Jahren.
        </p>
      </div>
      <div class="col-lg-3">
        <h3>Sitemap</h3>
        @if (has_nav_menu('footer_navigation'))
          {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => '']) !!}
        @endif
      </div>
      <div class="col-lg-3">
        <div class="copy">&copy; 2018</div>
        <a href="/">zahnarzt-thun-zentrum.ch</a>
        <div class="social">
          <a href="javascript;"><i class="fab fa-facebook"></i></a>
          <a href="javascript;"><i class="fab fa-linkedin"></i></a>
          <a href="javascript;"><i class="fab fa-twitter-square"></i></a>
        </div>
      </div>
    </div>

  </div>
</div>
