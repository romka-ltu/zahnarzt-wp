<div class="section section-vision">
  <div class="container">
    <div class="row">
      <div class="col-lg-8"></div>
      <div class="col-lg-4">
        <div class="vision-right">
          <h2>OUR VISION</h2>
          <div class="svg">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14">
              <defs>
                <filter id="services-sep">
                  <feFlood flood-color="#D14E45" flood-opacity="1" result="floodOut"/>
                  <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/>
                  <feBlend in="compOut" in2="SourceGraphic"/>
                </filter>
              </defs>
              <g filter="url(#services-sep)">
                <path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.999h32.312V8.67H45.685V6.999zM31.178 7.5l2.224-3.851 1.447.836-2.223 3.85-1.448-.835z"/>
                <path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.789 1.448-.836z"/>
                <path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.695l2.307-8.61 1.615.433-2.307 8.609-1.615-.432z"/>
                <path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.74l1.615-.433 3.01 11.233-1.615.432z"/>
                <image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGEhEGAKm2FFwAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/>
                <path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.999h32.312V8.67H.003V6.999z"/>
              </g>
            </svg>
          </div>
          <p class="section--subheading">
            Health care is the diagnosis, treatment,and
            prevention of disease, illness, injury, and other
            physical impairments in human beings.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="section section-services">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <h2>Services</h2>
        <div class="svg">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14">
            <defs>
              <filter id="services-sep">
                <feFlood flood-color="#D14E45" flood-opacity="1" result="floodOut"/>
                <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/>
                <feBlend in="compOut" in2="SourceGraphic"/>
              </filter>
            </defs>
            <g filter="url(#services-sep)">
              <path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.999h32.312V8.67H45.685V6.999zM31.178 7.5l2.224-3.851 1.447.836-2.223 3.85-1.448-.835z"/>
              <path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.789 1.448-.836z"/>
              <path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.695l2.307-8.61 1.615.433-2.307 8.609-1.615-.432z"/>
              <path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.74l1.615-.433 3.01 11.233-1.615.432z"/>
              <image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGEhEGAKm2FFwAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/>
              <path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.999h32.312V8.67H.003V6.999z"/>
            </g>
          </svg>
        </div>
        <p class="section--subheading">
          Health care is the diagnosis, treatment,and
          prevention of disease, illness, injury, and other
          physical impairments in human beings.
        </p>
        <a href="" class="btn btn-action text-white mt-4">Read more</a>
      </div>
      <div class="col-lg-8">
        <div class="row">
          <div class="col-md-6 p-4">
            <div class="section-services-col-wrap">
              <div>
                <div class="svg">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 931.843 931.843">
                    <path d="M926.932 305.137c12.301-38.3 1.4-86.6-32-124-14.5-16.2-31.4-28.5-49.199-36.5-5.1-8.4-11.201-16.6-18.1-24.4-29.9-33.5-69.4-51.5-105.701-51.7-25.4-19.5-59.699-34.3-98.699-40.4-30.7-4.8-60.3-3.6-86.2 2.4-22.5-9.8-49-13.8-76.8-10.3-28.899 3.7-54.5 14.9-74.1 31-31-14-70.9-14.8-108.9.9-32.7 13.5-57.8 36.5-72 63-25.4 5.6-51.1 19.1-73 40.1-31.1 29.8-47.5 68-47.4 102.8-37.8 21-61.8 57.1-60.1 95.6-10.1 15.4-17.7 33.5-21.7 53.4-6.9 34.5-1.7 67.899 12.3 94.3-1 58.3 31.7 108.6 80.9 118.4 3.2.6 6.4 1.1 9.6 1.399-1.7 12.101-1.5 24.4 1 36.7 14.1 71.1 95.7 114.8 182.3 97.6 3.899-.8 7.8-1.699 11.6-2.699 12.9 20.699 39.7 11.8 39.7 11.8v76.6c0 8.2 3.1 16 8.8 22l42.4 44.7c9.899 10.5 27.5 3.4 27.5-11v-86.6c2.6-78.101 31.3-116.7 55.8-131.4 33-19.9 32.5-61.3 32.3-76.4 26 13.7 67.101 21.5 101.5 17.601 155.199-17.9 163.299-122.3 168.4-139.4 45.299 3.4 86.898-15.8 104.898-52.7 2.5-5.199 4.5-10.6 5.9-16 15-8.899 27.301-21.6 35.1-37.6 10.602-21.601 11.301-46.001 3.901-69.201z" fill="#d14e45"/>
                  </svg>
                </div>
              </div>
              <div>
                <h3>NEUROLOGICAL</h3>
                <p>A neurologist is a physician specializing in neurology and trained to investigate.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 p-4">
            <div class="section-services-col-wrap">
              <div>
                <div class="svg">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 489.083 489.083">
                    <path d="M482.754 401.6l-48.9-48.9c15.1-20.7 23.9-46.3 23.9-73.9 0-15.8-2.9-30.9-8.3-44.9 29-60.3 21.7-137.2-19.8-187.1-25-30.2-58.3-46.8-94.7-46.8-65.8 0-102 55.1-102 55.1S197.654 0 132.054 0c-36.4 0-69.7 16.6-94.7 46.8-49.8 59.3-49.8 157.1.2 216.4l177.9 216.4c14.1 17.5 30.2 6.2 34.3 2.1l64.8-78.2c5.7.8 11.5 1.2 17.4 1.2 27.1 0 52.1-8.5 72.6-23.1l49.1 49.1c12.6 11.7 25 4.2 29.1 0 8.4-8.3 8.4-20.8 0-29.1zm-249.6 35.4l-164.4-198.7c-37.5-44.7-37.5-118.6 0-164.4 17.7-20.8 39.5-32.3 63.5-32.3 48.8 0 84.3 57.2 84.3 57.2 13.1 16.2 28.1 8.3 33.3 2.1 0 0 35-59.3 85.3-59.3 23.9 0 45.8 11.4 63.5 32.3 25.8 30.9 34.5 76.7 23.9 117.4-22.9-23.7-55-38.4-90.5-38.4-69.5 0-125.9 56.4-125.9 125.9 0 47.9 26.8 89.6 66.2 110.9l-39.2 47.3zm13.5-158.2c0-47.1 38.2-85.3 85.3-85.3s85.3 38.2 85.3 85.3-38.2 85.3-85.3 85.3-85.3-38.2-85.3-85.3z" fill="#d14e45"/>
                  </svg>
                </div>
              </div>
              <div>
                <h3>CARDIOLOGICAL</h3>
                <p>A cardiology is a branch of cardiology that deals specific with the catheter based.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 p-4">
            <div class="section-services-col-wrap">
              <div>
                <div class="svg">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.001 512.001">
                    <path d="M480.615 90.381c-14.047-40.775-42.729-70.776-78.693-82.31-68.293-21.898-124.454 6.242-145.92 19.733-21.466-13.492-77.629-41.632-145.921-19.733C74.1 19.61 45.411 49.629 31.37 90.429c-18.462 53.651-9.166 115.534 24.871 165.538 22.731 33.395 37.573 71.083 42.924 108.988.309 2.187.61 4.421.905 6.706 5.153 39.974 12.742 72.059 22.558 95.362 11.762 27.925 26.938 43.005 45.109 44.823 19.26 1.924 35.247-14.092 47.531-47.607 8.59-23.436 15.363-54.84 19.069-88.428 2.072-18.776 6.916-33.977 14.01-43.96 2.53-3.56 6.196-3.944 7.655-3.944 1.459 0 5.125.384 7.655 3.944 7.093 9.983 11.937 25.185 14.009 43.96 3.706 33.588 10.48 64.992 19.069 88.428 11.625 31.718 26.568 47.762 44.458 47.762 1.014 0 2.04-.052 3.073-.155 18.171-1.818 33.347-16.898 45.109-44.823 9.816-23.303 17.405-55.387 22.558-95.363.284-2.2.574-4.353.871-6.464 5.342-37.971 20.222-75.779 43.031-109.337 33.986-50.001 43.249-111.862 24.78-165.478z" fill="#d14e45"/>
                  </svg>
                </div>
              </div>
              <div>
                <h3>DENTAL CARE</h3>
                <p>Dental care is the maintenance of healthy teeth and may refer to Oral and Dentistry.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 p-4">
            <div class="section-services-col-wrap">
              <div>
                <div class="svg">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.001 512.001">
                    <path d="M256 96C144.341 96 47.559 161.021 0 256c47.559 94.979 144.341 160 256 160 111.656 0 208.439-65.021 256-160-47.559-94.979-144.344-160-256-160zm126.225 84.852c30.082 19.187 55.572 44.887 74.719 75.148-19.146 30.261-44.639 55.961-74.719 75.148C344.428 355.257 300.779 368 256 368c-44.78 0-88.428-12.743-126.225-36.852C99.695 311.96 74.205 286.26 55.058 256c19.146-30.262 44.637-55.962 74.717-75.148a235.143 235.143 0 0 1 5.929-3.65C130.725 190.866 128 205.613 128 221c0 70.691 57.308 128 128 128 70.691 0 128-57.309 128-128 0-15.387-2.725-30.134-7.703-43.799a240.633 240.633 0 0 1 5.928 3.651zM256 205c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48z" fill="#d14e45"/>
                  </svg>
                </div>
              </div>
              <div>
                <h3>Ophthalmology</h3>
                <p>Ophthalmology is the branch of medicine that deals with the diseases of the eye.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section section-specialists">
  <div class="container">

    <h2 class="text-center">OUR SPECIALISTS</h2>
    <div class="svg text-center">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14">
        <defs>
          <filter id="specialists-sep">
            <feFlood flood-color="#D14E45" flood-opacity="1" result="floodOut"/>
            <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/>
            <feBlend in="compOut" in2="SourceGraphic"/>
          </filter>
        </defs>
        <g filter="url(#specialists-sep)">
          <path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.999h32.312V8.67H45.685V6.999zM31.178 7.5l2.224-3.851 1.447.836-2.223 3.85-1.448-.835z"/>
          <path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.789 1.448-.836z"/>
          <path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.695l2.307-8.61 1.615.433-2.307 8.609-1.615-.432z"/>
          <path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.74l1.615-.433 3.01 11.233-1.615.432z"/>
          <image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGEhEGAKm2FFwAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/>
          <path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.999h32.312V8.67H.003V6.999z"/>
        </g>
      </svg>
    </div>
    <p class="section--subheading text-center">
      Medical staff service professionals work, along with medical and administrative leadership,
      to protect patients from incompetent, unskilled, impaired.
    </p>

    <div class="section-specialists-ppl">
      <div class="ppl-wrap">
        <div class="card-deck">
          <div class="card border-0 bg-transparent text-white text-center">
            <img class="card-img-top" src="@asset('images/doctor-1.jpg')" alt="">
            <div class="card-body">
              <h5 class="card-title">Dr. MARRY RODGERS</h5>
              <p class="card-text card-excerpt">DENTIST</p>
            </div>
          </div>
          <div class="card border-0 bg-transparent text-white text-center">
            <img class="card-img-top" src="@asset('images/doctor-2.jpg')" alt="">
            <div class="card-body">
              <h5 class="card-title">Dr. MARRY RODGERS</h5>
              <p class="card-text card-excerpt">DENTIST</p>
            </div>
          </div>
          <div class="card border-0 bg-transparent text-white text-center">
            <img class="card-img-top" src="@asset('images/doctor-3.jpg')" alt="">
            <div class="card-body">
              <h5 class="card-title">Dr. MARRY RODGERS</h5>
              <p class="card-text card-excerpt">DENTIST</p>
            </div>
          </div>
        </div>
      </div>
      <div class="bottom-line"></div>
    </div>

  </div>
</section>
