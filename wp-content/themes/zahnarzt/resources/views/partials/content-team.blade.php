<div class="section section-team">
    <div class="container">

        <h4>Zahnarzt thun zentrum</h4>
        <h2>EXPERIENCED TEAM</h2>
        <div class="svg">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14">
                <defs>
                  <filter id="team-sep">
                    <feFlood flood-color="#D14E45" flood-opacity="1" result="floodOut"/>
                    <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/>
                    <feBlend in="compOut" in2="SourceGraphic"/>
                  </filter>
                </defs>
                <g filter="url(#team-sep)">
                  <path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.999h32.312V8.67H45.685V6.999zM31.178 7.5l2.224-3.851 1.447.836-2.223 3.85-1.448-.835z"/>
                  <path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.789 1.448-.836z"/>
                  <path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.695l2.307-8.61 1.615.433-2.307 8.609-1.615-.432z"/>
                  <path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.74l1.615-.433 3.01 11.233-1.615.432z"/>
                  <image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGEhEGAKm2FFwAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/>
                  <path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.999h32.312V8.67H.003V6.999z"/>
                </g>
              </svg>
        </div>

        <div class="row" data-aos="fade-up">
            <div class="col-lg-6 order-last order-lg-first">
                <div class="member-bio-inner">
                    <div id="member_1" class="member-bio active">
                        <h3>Ana Sosunova</h3>
                        <small>Fachzahnärztin für Parodontologie</small>

                        <p><strong>Curriculum Vitae</strong></p>
                        <p>2016 Übernahme der Zahnarztpraxis von Dr. med. dent. Eva Kubin in Thun</p>
                        <p>2014 - 2016 Selbständige Zahnärztin in der Privatpraxis - Dr. med. dent. M. Sekulic, Willisau</p>
                        <p>2013 Anerkennung des zahnmedizinischen Diploms aus Litauen in der Schweiz </p>
                        <p>2011 - 2012 Fachzahnärztin für Parodontologie, verschiedene Privatpraxen.</p>
                        <p>2009 - 2011 Oberassistenzzahnärztin für Parodontologie, Departement für Parodontologie, Zalgirio Klinik der Universität Vilnius. Leitung: Dr. med. dent. J.Žekoniene</p>
                        <p>2009 - 2010 Oberassistenzzahnärztin für Zahn-Implantologie, implantations-Klinik Vilnius. Leitung: Dr. med. dent. A.Puišys</p>
                        <p>2007 - 2011 Weiterbildungsstudium für Parodontologie an der  Medizinische Fakultät der Universität Vilnius (3 Jahre Ausbildung, 10 dokumentierte Patienten, 3 Publikationen, Examen), Professor Dr. med. dent. A. Puriene</p>
                        <p>2007 - 2009 Assistenz Zahnärztin, verschiedene Praxen.</p>
                        <p>2007 Staatsexamen zur dipl. Zahnärztin an der Universität Kaunas, Litauen</p>
                        <p>2001 - 2006 Studium der Zahnmedizin an der Universität Kaunas, Litauen</p>
                        <p>1983 Geboren in Vilnius, Litauen</p>
                        <p><strong>Fortbildung</strong></p>
                        <p>Mit permanenten Fortbildungen in der allgemeinen wie der spezialisierten Zahnmedizin im In- und Ausland stellen wir sicher, Ihnen mit Hilfe neuester und bewährter Techniken den besten Service anbieten zu können - das heisst, nur das Beste für Sie ist gut genug.</p>
                        <p><strong>Sprachen</strong></p>
                        <p>Deutsch, Englisch, Russisch, Litauisch, Polnisch</p>
                    </div>
                    <div id="member_2" class="member-bio">
                        <h3>Daniela Regina Rolli</h3>
                        <small>Zahnarztassistentin</small>
                    </div>
                    <div id="member_3" class="member-bio">
                        <h3>Silvia Schneiter</h3>
                        <small>Zahnarztassistentin</small>
                    </div>
                    <div id="member_4" class="member-bio">
                        <h3>Margrit Schüpbach</h3>
                        <small>Zahnarztassistentin</small>
                    </div>
                </div>
                <div id="member_social_1" class="member-social active">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                    <a href=""><i class="fab fa-linkedin"></i></a>
                    <div class="phones">
                        <p><strong><i class="fas fa-phone-volume"></i> Termine:</strong> <a href="tel:+410332222209">+41 (0) 33 222 22 09</a></p>
                        <p><strong><i class="fas fa-phone-volume"></i> Notfälle:</strong> <a href="tel:+4100332262626">+41 (0) 033 226 26 26</a></p>
                    </div>
                </div>
                <div id="member_social_2" class="member-social">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                    <a href=""><i class="fab fa-linkedin"></i></a>
                </div>
                <div id="member_social_3" class="member-social">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                    <a href=""><i class="fab fa-linkedin"></i></a>
                </div>
                <div id="member_social_4" class="member-social">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                    <a href=""><i class="fab fa-linkedin"></i></a>
                </div>
            </div>
            <div class="col-lg-6 order-first order-lg-last">
                <div class="members">
                    <div class="photo">
                        <img src="" alt="">
                    </div>
                    <div class="members-list">
                        <ul>
                            <li data-member="1" class="active">
                                <div>
                                    Ana Sosunova<br>
                                    <small>Fachzahnärztin für Parodontologie</small>
                                </div>
                                <div>
                                    <img src="@asset('images/ana-s.jpg')" alt="">
                                </div>
                            </li>
                            <li data-member="2">
                                <div>
                                    Daniela Regina Rolli<br>
                                    <small>Zahnarztassistentin</small>
                                </div>
                                <div>
                                    <img src="@asset('images/ana-s.jpg')" alt="">
                                </div>
                            </li>
                            <li data-member="3">
                                <div>
                                    Silvia Schneiter<br>
                                    <small>Zahnarztassistentin</small>
                                </div>
                                <div>
                                    <img src="@asset('images/ana-s.jpg')" alt="">
                                </div>
                            </li>
                            <li data-member="4">
                                <div>
                                    Margrit Schüpbach<br>
                                    <small>Zahnarztassistentin</small>
                                </div>
                                <div>
                                    <img src="@asset('images/ana-s.jpg')" alt="">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div data-aos="fade-up" class="full-width specialists">
            <div class="row">
                <div class="col-lg-6 p-0">
                    <div class="full-background" style="background-image:url(@asset('images/667835192.jpg'))"></div>
                </div>
                <div class="col-lg-6 pt-5 pl-5">
                    <div class="specialists-right">
                        <h2>EXPERIENCED TEAM</h2>
                        <div class="svg">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14">
                                <defs>
                                <filter id="team-sep2">
                                    <feFlood flood-color="#ffffff" flood-opacity="1" result="floodOut"/>
                                    <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/>
                                    <feBlend in="compOut" in2="SourceGraphic"/>
                                </filter>
                                </defs>
                                <g filter="url(#team-sep2)">
                                <path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.999h32.312V8.67H45.685V6.999zM31.178 7.5l2.224-3.851 1.447.836-2.223 3.85-1.448-.835z"/>
                                <path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.789 1.448-.836z"/>
                                <path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.695l2.307-8.61 1.615.433-2.307 8.609-1.615-.432z"/>
                                <path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.74l1.615-.433 3.01 11.233-1.615.432z"/>
                                <image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGEhEGAKm2FFwAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/>
                                <path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.999h32.312V8.67H.003V6.999z"/>
                                </g>
                            </svg>
                        </div>
                        <p class="section--subheading pb-4">
                            Medical Services is the term used to describe the range of healthcare that is provided by General
                            Practitioners as part of the National Health Service in our Hospitals.
                        </p>

                        <ul>
                            <li>
                                <div>
                                    <div class="svg">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="84" height="83"><path stroke="#FFF" stroke-width="2" fill="none" d="M41.692.998c22.101 0 40.017 17.909 40.017 40 0 22.092-17.916 40.001-40.017 40.001-22.102 0-40.018-17.909-40.018-40.001 0-22.091 17.916-40 40.018-40z"></path><image x="26" y="29" width="33" height="30" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAeCAQAAAAIwb+cAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiBg4TACzUf+NwAAACaklEQVQ4y6WU3WuOcRjHP3u2ad6WsqzmhJK1zUSShCJZZEeokeRATpASCX+A4kQ7cUKUlyJ5yQ7QUF4XhSw2bMZmb96aPV6XbR8HuzfP89z3Myvf38l9/b7f63tf9/W77l+GBBhDGWuYwQR+0MgNztNKIgqoYBlTGE0XT7nENX4FjANrvg9Mxgd3GQvYXHf6MYnt966LBljEmOuMG0afpxwnTvKYUXjvpkGLcntMh2NmeTAt2+ZawUJrTI/3XrdrGP6xZbjd/0GPNTHK+R+MojjGrBFJq1lGdSTTy4jKbbNC3BjJfY6NqIZazgK1vIsiY/T/0+ATFwFooSraojW0108rLxPiOk4C8Jnr0RbPQnt1LGY1HUHUxQV+EiMLeMGjkDozxp2UrV9c5DUNHArieo4Dc9gDNHMlVPE7XOTXpA4/dJKIxT5Su90tjrfSJvPEFSnn8cZ9GY6mkDx6h3zbeRU8beAETyinnblcRbZxhrEUkUvfUBu+0DD4syevPKeLhd70qJjjPlWPRGrJECCHfHoxmLYC9pLNKrKZRgZ1lHKOQuAVW3hCzlAF0MV3giunwNNqt3HjdvtDfePy4C1Z7hj68t+BJu5Xe6xy3OB9gTjb+pRGXQ6YEmsjB/u5s/7eWgNrgS1Jko9uFjPdGmnQ6JLBzMTGLLUzSXbLTKd6L8LgrSv/5iV3d6nNCcK4pz0cYdBkWWJW6hHNC/UkFS9cmJwTPuci7w9jUGNJakbUsEz2bGR6n+fND+ujp3OC+0MG3zxoZpQ62gKzrLAjwaDN9WmUaS0QS70TGNx2ZnrdcBY40QN2Wun44VR/ANHiPOkeUtesAAAAAElFTkSuQmCC"></image></svg>
                                    </div>
                                </div>
                                <div>
                                    <h4>Neurological</h4>
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. A neque eaque, accusamus nobis vero pariatur, officia minima illum voluptas vitae assumenda hic?
                                        Totam exercitationem odit tempora deleniti quisquam, vel dignissimos.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div class="svg">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="84" height="83"><path stroke="#FFF" stroke-width="2" fill="none" d="M41.692.998c22.101 0 40.017 17.909 40.017 40 0 22.092-17.916 40.001-40.017 40.001-22.102 0-40.018-17.909-40.018-40.001 0-22.091 17.916-40 40.018-40z"></path><image x="26" y="29" width="33" height="30" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAeCAQAAAAIwb+cAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiBg4TACzUf+NwAAACaklEQVQ4y6WU3WuOcRjHP3u2ad6WsqzmhJK1zUSShCJZZEeokeRATpASCX+A4kQ7cUKUlyJ5yQ7QUF4XhSw2bMZmb96aPV6XbR8HuzfP89z3Myvf38l9/b7f63tf9/W77l+GBBhDGWuYwQR+0MgNztNKIgqoYBlTGE0XT7nENX4FjANrvg9Mxgd3GQvYXHf6MYnt966LBljEmOuMG0afpxwnTvKYUXjvpkGLcntMh2NmeTAt2+ZawUJrTI/3XrdrGP6xZbjd/0GPNTHK+R+MojjGrBFJq1lGdSTTy4jKbbNC3BjJfY6NqIZazgK1vIsiY/T/0+ATFwFooSraojW0108rLxPiOk4C8Jnr0RbPQnt1LGY1HUHUxQV+EiMLeMGjkDozxp2UrV9c5DUNHArieo4Dc9gDNHMlVPE7XOTXpA4/dJKIxT5Su90tjrfSJvPEFSnn8cZ9GY6mkDx6h3zbeRU8beAETyinnblcRbZxhrEUkUvfUBu+0DD4syevPKeLhd70qJjjPlWPRGrJECCHfHoxmLYC9pLNKrKZRgZ1lHKOQuAVW3hCzlAF0MV3giunwNNqt3HjdvtDfePy4C1Z7hj68t+BJu5Xe6xy3OB9gTjb+pRGXQ6YEmsjB/u5s/7eWgNrgS1Jko9uFjPdGmnQ6JLBzMTGLLUzSXbLTKd6L8LgrSv/5iV3d6nNCcK4pz0cYdBkWWJW6hHNC/UkFS9cmJwTPuci7w9jUGNJakbUsEz2bGR6n+fND+ujp3OC+0MG3zxoZpQ62gKzrLAjwaDN9WmUaS0QS70TGNx2ZnrdcBY40QN2Wun44VR/ANHiPOkeUtesAAAAAElFTkSuQmCC"></image></svg>
                                    </div>
                                </div>
                                <div>
                                    <h4>Neurological</h4>
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. A neque eaque, accusamus nobis vero pariatur, officia minima illum voluptas vitae assumenda hic?
                                        Totam exercitationem odit tempora deleniti quisquam, vel dignissimos.
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
