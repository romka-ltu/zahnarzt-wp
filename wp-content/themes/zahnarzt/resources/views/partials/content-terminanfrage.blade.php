@section('header-css-top')
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@stop

<div class="section section-visit">
  <div class="container">

    <div class="row">
      <div class="col-lg-12">
        <h2 class="text-center">GET APPOINTMENT</h2>
        <div class="svg text-center">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="14">
            <defs>
              <filter id="services-sep">
                <feFlood flood-color="#D14E45" flood-opacity="1" result="floodOut"/>
                <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut"/>
                <feBlend in="compOut" in2="SourceGraphic"/>
              </filter>
            </defs>
            <g filter="url(#services-sep)">
              <path fill-rule="evenodd" fill="#29B6F6" d="M45.685 6.999h32.312V8.67H45.685V6.999zM31.178 7.5l2.224-3.851 1.447.836-2.223 3.85-1.448-.835z"/>
              <path fill-rule="evenodd" fill="#29B6F6" d="M34.71 2.85l3.342 5.79-1.447.835-3.343-5.789 1.448-.836z"/>
              <path fill-rule="evenodd" fill="#29B6F6" d="M37.039 9.695l2.307-8.61 1.615.433-2.307 8.609-1.615-.432z"/>
              <path fill-rule="evenodd" fill="#29B6F6" d="M42.089 11.972L39.079.74l1.615-.433 3.01 11.233-1.615.432z"/>
              <image x="42" y="6" width="5" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAICAMAAAAGL8UJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAARVBMVEX////RTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkXRTkX///+rCqwCAAAAFXRSTlMAAxg5uTEBuEhVoA/Y7SCKeX3EEBUKE7uvAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IGEhEGAKm2FFwAAAAwSURBVAjXFYrJEcAgEICIt8Y72f5bdWWGFwCPQbHOQ4iSIEuBt7YOQ6a2tb+7/OoBFdQBEqiENIoAAAAASUVORK5CYII="/>
              <path fill-rule="evenodd" fill="#29B6F6" d="M.003 6.999h32.312V8.67H.003V6.999z"/>
            </g>
          </svg>
        </div>
        <p class="section--subheading text-center">
          Asunt  loreute irure dolor in reprehenderit in voluptate cillum luptate dolor in reprehendercil
          dolore eu  pariatur. Excepteur sint luptate cil dolor.
        </p>
      </div>
    </div>

    <div class="row" data-aos="fade-up">
      <div class="col-lg-8 offset-lg-2">
        <div class="visit-form-wrap">
          <div class="visit-form-heading">
            <h3>Enter your details</h3>
          </div>

          <form action="" method="post" id="visit_form">
            <div class="form-row">
              <div class="form-group col-lg-6">
                <label for="company_name">Firma</label>
                <input type="text" name="company_name" id="company_name" class="form-control">
              </div>
              <div class="form-group col-lg-6">
                <label for="gender">Anrede</label>
                <div class="dropdown">
                  <input type="hidden" name="gender">
                  <button class="btn btn-secondary dropdown-toggle" type="button" id="gender_select" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    -
                  </button>
                  <div class="dropdown-menu" aria-labelledby="gender_select">
                    <a class="dropdown-item" href="javascript:" data-name="gender" data-value="man">Herr</a>
                    <a class="dropdown-item" href="javascript:" data-name="gender" data-value="woman">Frau</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-lg-6">
                <label for="first_name">Vorname</label>
                <input type="text" name="first_name" id="first_name" class="form-control">
              </div>
              <div class="form-group col-lg-6">
                <label for="last_name">Nachname</label>
                <input type="text" name="last_name" id="last_name" class="form-control">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-lg-6">
                <label for="email">Email</label>
                <input type="text" name="email" id="email" class="form-control">
              </div>
              <div class="form-group col-lg-6">
                <label for="phone">Telefon</label>
                <input type="text" name="phone" id="last_name" class="form-control">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-lg-6">
                <label for="visit_date">Ihr Wunschtermin</label>
                <input type="text" name="visit_date" id="visit_date" class="form-control">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-lg-6">
                <label for="visit_msg">Ihr Wunschtermin</label>
                <textarea name="visit_msg" class="form-control" id="visit_msg" rows="5"></textarea>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-lg-6">
                <button type="submit"  class="btn btn-action text-white">Submit form</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>

  </div>
</div>
