<div class="section section-page @php post_class() @endphp">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        @php the_content() @endphp
      </div>
    </div>
  </div>
</div>
