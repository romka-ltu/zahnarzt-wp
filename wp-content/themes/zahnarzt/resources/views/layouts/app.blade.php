<!doctype html>
<html class="no-js" @php language_attributes() @endphp>
@include('partials.head')
<body @php body_class() @endphp>
  @yield('header-css-top')
  @php do_action('get_header') @endphp
  @yield('header-css-bottom')
  @include('partials.header')

  @yield('content')

  @php do_action('get_footer') @endphp
  @include('partials.footer')
  @php wp_footer() @endphp
</body>
</html>
