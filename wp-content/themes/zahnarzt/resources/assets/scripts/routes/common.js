import '../navigation'
import AOS from 'aos';

export default {
  init() {
    // JavaScript to be fired on all pages

    let $map = $('#map');
    let $show_map = $('.show_map');

    $("#navigation").navigation();

    AOS.init();

    if ( $map.length ) {

      $show_map.click(function(){
        $(this).closest('.section-map').toggleClass('showing_map');
        $(this).text(function(i, text){
          return text === "Show map" ? "Hide map" : "Show map";
        })
      });

      $map.lazyLoadGoogleMaps(
        {
          key: app.goog_api_key,
          callback: function( container, map )
          {
            let $container  = $( container ),
              marker      = new google.maps.LatLng( $container.attr( 'data-lat' ), $container.attr( 'data-lng' ) ),
              center      = new google.maps.LatLng( $container.attr( 'data-lat' ) - 0.00600000, $container.attr( 'data-lng' ) );

            map.setOptions({
              zoom: 15,
              center: center,
            });

            new google.maps.Marker({
              position: marker,
              map: map,
            });
          },
        });

    }

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
