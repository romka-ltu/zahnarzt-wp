import flatpickr from 'flatpickr';

export default {
  init() {

    flatpickr('#visit_date',{});



    let $dropdown_item = $('.dropdown-item');
    let $dropdown = $('.dropdown');
    let $gender_field = $("input[name='gender']");

    $dropdown_item.on('click', function(e) {

      e.preventDefault();

      let $this = $(this);
      let $button = $dropdown.find('button');

      $button.text( $this.text() );
      $gender_field.prop('value', $this.data('value'));

    })

  },
}
