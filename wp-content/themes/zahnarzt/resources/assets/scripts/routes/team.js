export default {
    init() {

        let $member = $('.members-list li');

        $member.on('click',function(){
            
            let $this = $(this);
            let memberId = $this.data('member');

            $('html, body').animate({
                scrollTop: $('.member-bio-inner').offset().top - 32,
            }, 500);

            $('.members-list li').removeClass('active');
            $('.member-bio').removeClass('active');
            $('.member-social').removeClass('active');

            $this.addClass('active');
            $('#member_'+memberId).addClass('active');
            $('#member_social_'+memberId).addClass('active');

        });

    },
};
