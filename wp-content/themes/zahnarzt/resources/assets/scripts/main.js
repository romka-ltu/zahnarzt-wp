// import external dependencies
import 'jquery';

// Import everything from autoload
import "./autoload/**/*"

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import team from './routes/team';
import terminanfrage from './routes/terminanfrage';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  home,
  aboutUs,
  team,
  terminanfrage,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
